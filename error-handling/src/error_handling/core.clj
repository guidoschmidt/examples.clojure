;;; https://jakemccrary.com/blog/2018/02/18/using-clojure-macros-for-nicer-error-handling/

(ns error-handling.core
  (:gen-class)
  (require '[clj-http.client :refer [get]]))

(defrecord Stopper [x])

(defn halt [data]
  (Stopper. data))

(defmacro halt-on-error->> [form & forms]
  (let [g (gensym)
        pstep (fn [step] `(if (instance? Stopper ~g) ~g (->> ~g ~step)))]
    `(let [~g ~form
           ~@(interleave (repeat g) (map pstep forms))]
       (if (instance? Stopper ~g)
         (.x ~g)
         ~g))))

(defn validate-required-fields [params]
  (when-not (contains? params :source)
    "Missing source field"))

(defn validate-invariants [params]
  (when (>= (:lower params) (:higher params))
    "Lower field must be smaller than higher"))

(defn simulate-request [req]
  (let [params (:params req)]
    (if-let [field-error (validate-required-fields params)]
      {:status 400 :body field-error}
      (if-let [invariant-error (validate-invariants params)]
        {:status 400 :body invariant-error}
        {:status 200 :body "Success"}))))

(simulate-request {:params {:higher 3 :lower 2 :source "http://duck.go"}})

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (simulate-request {:params {:higher 3 :lower 1 :source "Test"}}))
