;; SRC: https://blog.frankel.ch/learning-clojure/1/
(ns spec.core
  (:require [clojure.spec.alpha :as spec]))

;; clojure.spec.alpha is available since Clojure 1.9

(spec/def ::nil nil?)
(spec/def ::bool boolean?)
(spec/def ::string string?)

;; Using `::` syntax means that the keywords which are used with it
;; are resolved using the current namespace:
;; e.g.
;; `::nil` is resolved as `:spec.core/bool`

;; One can use enumerations as well:
(spec/def ::direction #{::NORTH ::EAST ::WEST ::SOUTH})

;; Check specs by using the `valid?` function:
(spec/valid? ::nil nil)
(spec/valid? ::nil 0)

(spec/valid? ::bool true)
(spec/valid? ::bool false)
(spec/valid? ::bool nil)

(spec/valid? ::string "A string.")
(spec/valid? ::string #"Nope")

;; Using `conform?` will return the value if the argument passed
;; conforms to the spec and `clojure.spec.alpha/invalid` otherwise
(spec/conform ::nil nil)
(spec/conform ::nil 0)

(spec/conform ::string "LOL")
(spec/conform ::string 'error)

;; Write custom spec functions
(defn local-date?
  "Check if the parameter is a java.time.LocalDate instance."
  [x]
  (instance? java.time.LocalDate x))

(spec/def ::date local-date?)

(spec/valid? ::date (java.time.LocalDate/of 2009 1 1))
(spec/valid? ::date "f")

;; Spec'ing complex data structures
(spec/def ::first-name string?)
(spec/def ::last-name string?)
(spec/def :birthdate local-date?)

(spec/def ::person (spec/keys :req [::first-name ::last-name]
                              :opt [::birthdate]))

(spec/valid? ::person {::first-name "John"
                       ::last-name "Doe"
                       ::birthdate (java.time.LocalDate/of 1970 12 1)})
                       

(spec/valid? ::person {::first-name "John"
                       ::last-name "Doe"
                       ::birthdate (java.time.LocalDate/of 1970 12 1)
                       ::title "Dr."})

;; Spec'ing collections
(spec/def ::dates (spec/coll-of ::date))
(spec/def ::map-dates (spec/map-of keyword? ::date))
(spec/def ::map-persons (spec/map-of keyword? ::person))

(spec/valid? ::dates [(java.time.LocalDate/of 1990 9 2)])
(spec/valid? ::dates #{:now (java.time.LocalDate/of 2020 1 1)})

(spec/valid? ::map-dates {:now (java.time.LocalDate/of 2012 12 2)})
(spec/valid? ::map-dates #{:now (java.time.LocalDate/of 2012 12 2)})

(spec/valid? ::map-persons {:john {::first-name "John"
                                   ::last-name "Doe"}})

(spec/valid? ::map-persons {:john {::first-name "Johnnie"}})
